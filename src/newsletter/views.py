from django.shortcuts import render
from .forms import ContactForm, SignUpForm
from .models import SignUp

def home(request):
    title = 'Sign Up Now'
    form = SignUpForm(request.POST or None)
    context = {"title": title, "form": form}
    if form.is_valid():
        instance = form.save()#ModelForm can be saved into db like this
        title = 'Thank you'
        context['title'] = title
    if request.user.is_authenticated and request.user.is_staff:
        qrset = SignUp.objects.all().order_by('-timestamp').filter(full_name__icontains='S')
        print(qrset)
        context = {
            "queryset": qrset,
        }


    return render(request, "home.html", context)

def contact(request):
    title = 'Contact Us'
    form = ContactForm(request.POST or None)
    if form.is_valid():
        for key, val in form.cleaned_data.iteritems():
            print key, val
        # email = form.cleaned_data.get('email')
        # message = form.cleaned_data.get('message')
        # full_name = form.cleaned_data.get('full_name')

    context = {"form": form, "title": title}
    return render(request, "forms.html", context)
